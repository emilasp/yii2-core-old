<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 11.10.14
 * Time: 2:17
 */

namespace emilasp\core\behaviors;

use emilasp\core\helpers\EStringHelper;
use yii;
use yii\base\Behavior;

/** Получаем и сохраняем данные из поля json модели
 * Class DataBehavior
 * @package emilasp\core\behaviors
 */

class DataBehavior extends Behavior
{
    /** Имя поля в БД
     * @var string
     */
    public $attribute = 'data_json';

    public function getData( $name = null ){
        $data = yii\helpers\Json::decode($this->owner->{$this->attribute});

        if(!is_array($data) && is_null($name) ) return [];
        if(!is_null($name)&&( !is_array($data) || !isset($data[$name])) ) return 0;

        if(!is_null($name)) return $data[$name];

        return $data;
    }

    /**
     * @param array $data - данные в виде массива
     * @param bool $save сохранять модель
     */
    public function setData( $data ,$save = true){
        $this->owner->{$this->attribute} = yii\helpers\Json::encode($data);

        if($save) $this->owner->save();
    }

}
