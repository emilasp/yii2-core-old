

$( document ).ready(function() {
    $('.tooltip-click').jBox('Tooltip', {
        trigger: 'click',
        animation: 'zoomIn'
    });
    $('.tooltip-hover-lt').jBox('Tooltip',{animation: 'zoomIn',position: { x: 'left', y: 'top' }});
    $('.tooltip-hover-rt').jBox('Tooltip',{animation: 'zoomIn',position: { x: 'right', y: 'top' }});
    $('.tooltip-hover-lb').jBox('Tooltip',{animation: 'zoomIn',position: { x: 'left', y: 'bottom' }});
    $('.tooltip-hover-rb').jBox('Tooltip',{animation: 'zoomIn',position: { x: 'left', y: 'bottom' }});
    $('.tooltip-hover').jBox('Tooltip',{animation: 'zoomIn'});

});