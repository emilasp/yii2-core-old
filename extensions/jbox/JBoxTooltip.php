<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 14.08.14
 * Time: 17:43
 */

namespace emilasp\core\extensions\jbox;

use yii;

class JBoxTooltip  extends JBox {

    public $type = self::TYPE_TOOLTIP;
    //public $idButton = 'modal';
    //public $idContent = false;

    public $clientOptions = [];

    public $title = '';

    private $_clientOptions = [
        'addClass' => 'jbox-tooltip',
        'animation' => 'flip',
        //'theme' => 'BorderBlue'
    ];



    public function init(){
        $this->_clientOptions['title'] = $this->title;

        $this->clientOptions = yii\helpers\ArrayHelper::merge($this->_clientOptions,$this->clientOptions);

        parent::init();
    }

    public function run(){
        //
        parent::run();
    }


}
