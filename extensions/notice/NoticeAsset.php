<?php

namespace emilasp\core\extensions\notice;
use kartik\widgets\AssetBundle;

class NoticeAsset extends AssetBundle
{

    public $jsOptions=['position'=>1];

    //public $sourcePath = 'emilasp/account/extensions/AdminSkin/assets/';

    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset'
    ];

    public function init()
    {
        $this->setSourcePath(__DIR__ . '/assets');
        //$this->setupAssets('js', ['alert','admin']);
        //$this->setupAssets('css', ['alert','admin']);
        parent::init();
    }


}
