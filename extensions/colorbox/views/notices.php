<?php

use kartik\widgets\Growl;
$flashs = Yii::$app->session->allFlashes;

$user = Yii::$app->getUser()->id;

$index = 0;
foreach( $flashs as $type=>$text){
    $index++;

    if(!isset(\emilasp\core\components\OptionsData::$messages[$type]))
        $type = \emilasp\core\components\OptionsData::MESSAGE_INFO;

    emilasp\core\extensions\jbox\JBoxNotice::widget([
            'type'=>$type,
            'delayOpen'=>$index*600,
            //'title'=>\emilasp\core\components\OptionsData::$messages[$type],
            'content'=>$text
        ]);


    /*echo Growl::widget([
            'type' => Growl::TYPE_INFO,
            'title' => \emilasp\core\components\OptionsData::$messages[$type],
            'icon' => 'glyphicon glyphicon-'.emilasp\core\extensions\alert\Alert::$icons[$type] ,
            'body' => $text,
            'showSeparator' => false,
            'delay' => 1000,
            'pluginOptions' => [
                //'icon_type'=>'image',
                'placement' => [
                    'from' => 'top',
                    'align' => 'right',
                ],
                //'template' => [
                  //  'icon_type' => 'image'
                //]
            ]
        ]);*/
}