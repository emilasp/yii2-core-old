<?php
namespace emilasp\core\extensions\colorbox;

use yii\web\AssetBundle;

class ColorboxAsset extends AssetBundle
{
    public $sourcePath = '@bower/jquery-colorbox';
    public $depends = [
       /* 'yii\bootstrap\BootstrapPluginAsset',
        'Zelenin\yii\widgets\Summernote\FontawesomeAsset',
        'Zelenin\yii\widgets\Summernote\CodemirrorAsset',*/
    ];

    public function init()
    {
        $this->css[] = 'example'.\Yii::$app->getModule('core')->modalSkin.'/colorbox.css';
        $this->js[] = YII_DEBUG
            ? 'jquery.colorbox.js'
            : 'jquery.colorbox-min.js';

        parent::init();
    }
}
