<?php

namespace emilasp\core\components;

use emilasp\core\helpers\EStringHelper;
use emilasp\front\user\models\FrontUser;
use sitkoru\cache\ar\ActiveRecordTrait;
use Yii;


class ActiveRecord extends \yii\db\ActiveRecord
{


    public $searched = false;

    public function beforeValidate()
    {
        if (parent::beforeValidate()) {
            if(!$this->searched){
                $this->onBeforeValidate();
            }
            return true;
        }
        return false;
    }


    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->onBeforeSave();
            return true;
        }
        return false;
    }

    public function afterSave($insert, $changedAttributes)
    {
        $this->onAfterSave();
        return parent::afterSave($insert, $changedAttributes);
    }

    /**
     * Проверяем имеет ли модель атрибут
     * @param string $attribute
     * @return bool
     */
    public function isAttr( $attribute ){
        return array_key_exists($attribute,$this->attributes);
    }

    /**
     * Деействия перед валидацией
     */
    private function onBeforeValidate(){

        if ( $this->isAttr('datecreate') && $this->isNewRecord ) {
            // generate password with length 6
            $this->datecreate = date('Y-m-d H:i:s');
        }
        if ( $this->isAttr('dateupdate') ) {
            // generate password with length 6
            $this->dateupdate = date('Y-m-d H:i:s');
        }
        if ( $this->isAttr('avtor_id') && ($this->isNewRecord || is_null($this->avtor_id))) {
            // generate password with length 6
            $this->setAvtorId();
        }
    }

    /**
     * Действия перед сохранением
     */
    private function onBeforeSave(){

    }
    /**
     * Действия после сохранением
     */
    private function onAfterSave(){

    }

    private function setAvtorId(){
        if( Yii::$app->id=='app-backend' ){
            if( in_array(EStringHelper::getClassName($this), Yii::$app->params['setAvtor']['models'])){
                $this->avtor_id = Yii::$app->bd->cache(function ($db) {
                        return $db->createCommand('SELECT id FROM front_user_user WHERE email="'.Yii::$app->params['setAvtor']['email'].'"')->scalar();
                    }, 36000);
                return;
            }
        }
        $this->avtor_id = Yii::$app->user->id;
    }

}
