<?php

namespace emilasp\core\components;

use Yii;
use yii\base\Component;
use yii\log\Logger;
use yii\validators\EmailValidator;

/**
 * Компонент для отправки почты
 *
 * Class Accounts
 * @package emilasp\core\components
 */
class CoreComponent extends Component
{

    const ALERT_SUCCESS = 'success';
    const ALERT_INFO = 'info';
    const ALERT_ERROR = 'error';

    /**
     * Включение отправки сообщений
     * @var bool
     */
    public $enableEmail = true;
    /**
     * Сохранение отправленных писем в базу
     * @var bool
     */
    public $saveEmailInBd = false;

    public function init()
    {



    }


    /**Отправка алерта юзеру
     * @param string $message
     * @param string $type
     */
    public function alert($message, $type = self::ALERT_INFO ){
        \Yii::$app->getSession()->setFlash($type,$message);
    }

    /**
     * Запись в лог
     * @param $message
     * @param bool $flash
     * @param int $level
     */
    public function addLog($message, $flash = false, $level = Logger::LEVEL_INFO){
        Yii::getLogger()->log($message, $level, 'binary');
        if( $flash ){
            if( $level == Logger::LEVEL_ERROR )
                $this->alert($message,self::ALERT_ERROR);
            else
                $this->alert($message,self::ALERT_SUCCESS);
        }
    }

    /**
     * Отправляем почту
     * @param string $subject
     * @param array $adresats ['user@somehost.com', 'John Smith']
     * @param bool|string $body
     * @param bool|string $view
     * @param bool|array $viewParams
     */
    public function sendMail($subject, $adresats, $body, $view = false, $viewParams = []){

        if( !$this->enableEmail || !(new EmailValidator)->validate($adresats[0]) ) return;

        if($view)
            $mail = Yii::$app->mail->compose( ['html'=>$view], $viewParams);
        else
            $mail = Yii::$app->mail->compose();

        $send = $mail->setTo($adresats[0])
            ->setFrom([Yii::$app->params['adminEmail'] => 'admin'])
            ->setSubject($subject)
            ->send();

        if(!$send)
            $this->addLog('Ошибка отправки письма',true, Logger::LEVEL_ERROR);

    }

    public function createTask( $type, $params = []){

    }



}
