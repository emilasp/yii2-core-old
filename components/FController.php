<?php

namespace emilasp\core\components;

use Yii;
use yii\web\Controller;

/**
 * Site controller
 */
class FController extends  Controller
{

    public function addMetaTags($title, $keywords, $description){
        if($keywords && strlen($keywords)>5)
            $this->view->registerMetaTag([ 'name' => 'keywords', 'content' => $keywords ]);
        if($description && strlen($description)>5)
            $this->view->registerMetaTag([ 'name' => 'description', 'content' => \emilasp\core\helpers\EStringHelper::toMeta($description) ]);
        if($title && strlen($title)>5)
            $this->view->title = $title;
    }

    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {

        if (parent::beforeAction($action)) {
            $request = Yii::$app->getRequest();
            // исключаем страницу авторизации или ajax-запросы
            if (!($request->getIsAjax() || $request->getIsPost() || strpos($request->getUrl(), 'login') !== false)) {
                Yii::$app->user->setReturnUrl($request->getUrl()); //Yii::$app->user->setPrevUrls($request->getUrl());
            }
            return true;
        } else {
            return false;
        }
    }

    /*public function afterAction($action, $result)
    {

        if (parent::afterAction($action, $result)) {
            $request = Yii::$app->getRequest();
            // исключаем страницу авторизации или ajax-запросы
            if (!($request->getIsAjax() || $request->getIsPost() || strpos($request->getUrl(), 'login') !== false)) {
                Yii::$app->user->setPrevUrls($request->getUrl());
            }
            return $result;
        } else {
            return false;
        }
    }*/

}
