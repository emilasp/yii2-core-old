<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 08.12.13
 * Time: 1:57
 */
namespace emilasp\core\components;

class OptionsData extends \yii\base\Component {
    /**
     * Option consts
     */
    const OPT_TOREPLACE = "--|replace|--";


    /**
     * Основные типы сообщений/нотисов
     */
    const MESSAGE_SUCCESS = 'success';
    const MESSAGE_INFO = 'info';
    const MESSAGE_ERROR = 'error';
    const MESSAGE_USER = 'user';

    public static $messages = array(
        self::MESSAGE_SUCCESS=>'Успех',
        self::MESSAGE_INFO=>'Информация',
        self::MESSAGE_ERROR=>'Ошибка',
        self::MESSAGE_USER=>'Лично',
    );
    /**
     * Publication options
     */
    const STATUS_close = 0;
    const STATUS_open = 1;
    const STATUS_moder = 2;
    const STATUS_delete = 3;

    public static $status = array(
        self::STATUS_close=>'Скрыто',
        self::STATUS_open=>'Опубл.',
        self::STATUS_moder=>'На модер.',
        self::STATUS_delete=>'Удалено',
    );



    /**
     * Pages types
     */
    const PAGES_TYPE_FOR_USER = 0;
    const PAGES_TYPE_FOR_AUTH_USER = 1;
    const PAGES_TYPE_FOR_ADMIN = 2;

    public static $pages_type = array(
        self::PAGES_TYPE_FOR_USER=>'Для всех',
        self::PAGES_TYPE_FOR_AUTH_USER=>'Для авторизованных',
        self::PAGES_TYPE_FOR_ADMIN=>'Для админки',
    );

    /**
     * Posts types
     */
    const POSTS_TYPE_POSTS = 0;

    public static $posts_type = array(
        self::POSTS_TYPE_POSTS=>'Посты',
    );

    /**
     * Posts types
     */
    const NEWS_TYPE= 0;

    public static $news_type = array(
        self::NEWS_TYPE=>'Новости',
    );


    /**
     * Options types
     */
    const OPTIONS_TYPE_OPTION   = 0;
    const OPTIONS_TYPE_POSTS    = 1;
    const OPTIONS_TYPE_NEWS     = 2;
    const OPTIONS_TYPE_PAGE     = 3;
    const OPTIONS_TYPE_CATEGORY = 4;
    const OPTIONS_TYPE_PRICES   = 5;
    const OPTIONS_TYPE_USLUGI   = 6;

    public static $options_type = array(
        self::OPTIONS_TYPE_OPTION =>'Опция',
        self::OPTIONS_TYPE_POSTS =>'Статья',
        self::OPTIONS_TYPE_NEWS =>'Новость',
        self::OPTIONS_TYPE_PAGE =>'Страница',
        self::OPTIONS_TYPE_CATEGORY =>'Категория',
        self::OPTIONS_TYPE_PRICES =>'Цена',
        self::OPTIONS_TYPE_USLUGI =>'Услуга',
    );


    /**
     * Category types
     */
    const CATEGORY_TYPE_CATEGORY= 1;
    const CATEGORY_TYPE_NEWS= 2;
    const CATEGORY_TYPE_POST = 3;
    const CATEGORY_TYPE_TATTOO_IMAGE = 4;
    const CATEGORY_TYPE_FILE = 5;

    public static $category_type = array(
        self::CATEGORY_TYPE_CATEGORY =>'Категория',
        self::CATEGORY_TYPE_NEWS =>'Новость',
        self::CATEGORY_TYPE_POST =>'Статья',
        self::CATEGORY_TYPE_TATTOO_IMAGE =>'Татуировка',
        self::CATEGORY_TYPE_FILE =>'Файл',
    );

    /**
     * Tags model types
     */
    const TAG_MODEL_TYPE_TATTOO = 1;

    public static $tagmodel_type = array(
        self::TAG_MODEL_TYPE_TATTOO =>'Татуировка',
    );
    /**
     * Tags types
     */
    const TAG_TYPE_TATTOO = 1;
    const TAG_TYPE_POSTS = 2;
    const TAG_TYPE_NEWS = 3;

    public static $tag_type = array(
        self::TAG_TYPE_TATTOO =>'Татуировка',
        self::TAG_TYPE_POSTS =>'Статьи',
        self::TAG_TYPE_NEWS =>'Новости',
    );

    /**
     * Tattoo Statuses
     */
    /**
     * Publication options
     */
    const TATTOO_STATUS_HIDE = 0;
    const TATTOO_STATUS_VISIBLE = 1;
    const TATTOO_STATUS_MODERATION = 2;
    const TATTOO_STATUS_DELETE = 3;

    public static $tattoo_status = array(
        self::TATTOO_STATUS_HIDE=>'Скрыто',
        self::TATTOO_STATUS_VISIBLE=>'Опубл.',
        self::TATTOO_STATUS_MODERATION=>'На модер.',
        self::TATTOO_STATUS_DELETE=>'Удалено',
    );

    const RATING_TYPE_FIVE = 0;
    const RATING_TYPE_ZA = 1;


    public static $rating_type = array(
        self::RATING_TYPE_FIVE=>'Пять звёзд',
        self::RATING_TYPE_ZA=>'За',
    );
    /**
     * User type options
     */
   /* const USER_TYPE_salon = 0;
    const USER_TYPE_master = 1;
    const USER_TYPE_user = 2;
    const USER_TYPE_moderator = 3;

    public static $user_type = array(
        self::USER_TYPE_salon=>'Салон',
        self::USER_TYPE_master=>'Мастер',
        self::USER_TYPE_user=>'Пользователь',
        self::USER_TYPE_moderator=>'Модератор',
    );*/
    /**
     * Content managment task type options
     */
   /* const MCONTENT_TYPE_parseflickr = 0;
    const MCONTENT_TYPE_parsesite = 1;
    const MCONTENT_TYPE_parsefolder = 2;
    const MCONTENT_TYPE_salons = 3;

    public static $mcontent_type = array(
        self::MCONTENT_TYPE_parseflickr=>'Img Flickr',
        self::MCONTENT_TYPE_parsesite=>'Img From site',
        self::MCONTENT_TYPE_parsefolder=>'Img Parse Folder',
        self::MCONTENT_TYPE_salons=>'Salon Parse',
    );*/
    /**
     * Publication options
     */
    /*const PUB_MContentAdded_dorab = 2;
    const PUB_MContentAdded_good = 1;
    const PUB_MContentAdded_moder = 0;

    public static $pub_mcontent_added = array(
        self::PUB_MContentAdded_dorab=>'На доработку',
        self::PUB_MContentAdded_good=>'Отлично',
        self::PUB_MContentAdded_moder=>'На модерации',
    );*/
    /**
     * Publication options
     */
   /* const PUB_MContent_notuser = 0;
    const PUB_MContent_job = 1;
    const PUB_MContent_moder = 2;
    const PUB_MContent_complete = 3;
    const PUB_MContent_oplata = 4;

    public static $pub_mcontent = array(
        self::PUB_MContent_notuser=>'Не назначен',
        self::PUB_MContent_complete=>'Ждёт оплаты',
        self::PUB_MContent_moder=>'На модерации',
        self::PUB_MContent_job=>'В работе',
        self::PUB_MContent_oplata=>'Выполнен',
    );*/
    /**
     * Publication options
     */
    /*const TYPE_MContentAdded_good = 1;
    const TYPE_MContentAdded_aftedorab = 0;


    public static $type_mcontent_added = array(
        self::TYPE_MContentAdded_aftedorab=>'После доработки',
        self::TYPE_MContentAdded_good=>'Отлично',
    );*/
    /**
     * Parse status
     */
    /*const PARSE_STATUS_bad = 0;
    const PARSE_STATUS_onsite = 2;
    const PARSE_STATUS_new= 1;

    public static $parseImageStatus = array(
        self::PARSE_STATUS_bad=>'Не ищем',
        self::PARSE_STATUS_onsite=>'На сайте',
        self::PARSE_STATUS_new=>'На модерации',
    );*/

    /**
     * Parse status
     */
    /*const MODELS_MContent_galleryFoto = 'GalleryFoto';
    const MODELS_MContent_company = 'Company';
    const MODELS_MContent_posts= 'Posts';
    const MODELS_MContent_comments= 'Comments';

    public static $models_mcontent = array(
        self::MODELS_MContent_galleryFoto=>'Татуировки',
        self::MODELS_MContent_company=>'Салоны',
        self::MODELS_MContent_posts=>'Статьи',
        self::MODELS_MContent_comments=>'Комментарии',
    );*/

    /**
     * Tasks status
     */
    /*const TASKS_new = 0;
    const TASKS_in_progress= 1;
    const TASKS_complete= 2;
    const TASKS_delete= 3;
    const TASKS_fail= 4;

    public static $tasks_status = array(
        self::TASKS_new=>'Новая',
        self::TASKS_in_progress=>'В работе',
        self::TASKS_complete=>'Завершена',
        self::TASKS_delete=>'Удалена',
        self::TASKS_fail=>'С ошибкой',
    );*/
    /**
     * Tasks type
     */
    /*const TASKS_type_exec = 1;
    const TASKS_type_url = 2;
    const TASKS_type_norm= 0;

    public static $tasks_type = array(
        self::TASKS_type_exec=>'Команда',
        self::TASKS_type_url=>'Ссылка',
        self::TASKS_type_norm=>'Нормал',
    );*/
    /**
     * Tasks type
     */
    /*const TASKS_queue_imgparseFolder = 1;
    const TASKS_queue_imgparseHtml = 3;
    const TASKS_queue_imgparseListFile = 4;
    const TASKS_queue_imgparseAdd = 2;
    const TASKS_queue_mail= 0;

    public static $tasks_queue = array(
        self::TASKS_queue_imgparseFolder=>'Парсер папка',
        self::TASKS_queue_imgparseHtml=>'Парсер файл html',
        self::TASKS_queue_imgparseListFile=>'Парсер файл с урлами',
        self::TASKS_queue_imgparseAdd=>'Ген(1 img).',
        self::TASKS_queue_mail=>'Почта',
    );*/

    /**
     * Bot periods
     */
    /*const BOT_PERIOD_M_1 = 1;
    const BOT_PERIOD_M_5 = 2;
    const BOT_PERIOD_M_15 = 3;
    const BOT_PERIOD_M_30 = 4;
    const BOT_PERIOD_H_1 = 5;
    const BOT_PERIOD_H_3 = 6;
    const BOT_PERIOD_H_6 = 7;
    const BOT_PERIOD_H_12 = 8;
    const BOT_PERIOD_D_1 = 9;
    const BOT_PERIOD_W_1 = 10;
    const BOT_PERIOD_MONTH_1 = 11;
    const BOT_PERIOD_MONTH_6 = 12;

    public static $bot_periods = array(
        self::BOT_PERIOD_M_1=>'1 минута',
        self::BOT_PERIOD_M_5=>'5 минут',
        self::BOT_PERIOD_M_15=>'15 минут',
        self::BOT_PERIOD_M_30=>'30 минут',
        self::BOT_PERIOD_H_1=>'1 час',
        self::BOT_PERIOD_H_3=>'3 часа',
        self::BOT_PERIOD_H_6=>'6 часов',
        self::BOT_PERIOD_H_12=>'12 часов',
        self::BOT_PERIOD_D_1=>'1 сутки',
        self::BOT_PERIOD_W_1=>'1 неделя',
        self::BOT_PERIOD_MONTH_1=>'1 месяц',
        self::BOT_PERIOD_MONTH_6=>'6 месяцев',
    );*/


    /**
     * Bot periods
     */
    /*const BOT_MARKET_CRYPTSY = 1;

    public static $bot_markets = array(
        self::BOT_MARKET_CRYPTSY=>'Cryptsy',
    );*/


    /**
     * Bot rule actions
     */
    /*const BOT_RULE_ACTIONS_SELL = 0;
    const BOT_RULE_ACTIONS_BAY = 1;
    const BOT_RULE_ACTIONS_NOT_SELL = 2;
    const BOT_RULE_ACTIONS_NOT_BAY = 3;
    const BOT_RULE_ACTIONS_SEND_MAIL = 4;

    public static $bot_rule_actions = array(
        self::BOT_RULE_ACTIONS_SELL=>'Продавать',
        self::BOT_RULE_ACTIONS_BAY=>'Покупать',
        self::BOT_RULE_ACTIONS_NOT_SELL=>'Не продавать',
        self::BOT_RULE_ACTIONS_NOT_BAY=>'Не покупать',
        self::BOT_RULE_ACTIONS_SEND_MAIL=>'Уведомление',
    );*/

    /**
     * Bot settings type
     */
    /*const BOT_SETTINGS_TYPE_INT = 0;
    const BOT_SETTINGS_TYPE_STRING = 1;
    const BOT_SETTINGS_TYPE_FLOAT= 2;

    public static $bot_settings = array(
        self::BOT_SETTINGS_TYPE_INT=>'Число цел.',
        self::BOT_SETTINGS_TYPE_STRING=>'Строка',
        self::BOT_SETTINGS_TYPE_FLOAT=>'Число плав.',
    );*/

    /**
     * Bot bot settings class
     */
    /*const BOT_SETTINGS_CLASS_HISTORY_PROJECTS = 'HistoryCommand';
    const BOT_SETTINGS_CLASS_HISTORY_ALL = 'HistoryAllCommand';
    const BOT_SETTINGS_CLASS_HISTORY_CALC= 'HistoryCalcCommand';
    const BOT_SETTINGS_CLASS_BOT = 'BotCommand';


    public static $bot_settings_class = array(
        self::BOT_SETTINGS_CLASS_HISTORY_PROJECTS=>'Пары проекты',
        self::BOT_SETTINGS_CLASS_HISTORY_ALL=>'Пары все',
        self::BOT_SETTINGS_CLASS_HISTORY_CALC=>'Калькуляция',
        self::BOT_SETTINGS_CLASS_BOT=>'Бот',
    );*/

///////////////
////////
//

    /**
     * @param $name
     * @param $option
     * @return string
     */
    public static function getValue($name,$option){
        if(!isset(self::$$name)) return "--";
        $var = self::$$name;
        if(!isset($var[$option])) return "--";
        $val = $var[$option];
        return $val;
    }

    /**
     * @param $name
     * @param bool $except
     * @return array
     */
    public static function getOptions($name,$except=false){
        $filter = array();
        foreach(self::$$name as $key=>$option){
            if(is_array($except))  {
                if(in_array($key,$except)) {
                    continue;
                }
            }
            $filter[$key] = $option;
        }
        return $filter;
    }
}
