<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 29.09.14
 * Time: 21:24
 */

namespace emilasp\core\helpers;


use yii\helpers\FormatConverter;


class EFormatterHelper extends FormatConverter{


    public static  $month = [
        'ru_RU'=>[
            'Января', 'Февраля', 'Марта', 'Апреля', 'Мая', 'Июня', 'Июля', 'Августа', 'Сентября', 'Октября', 'Ноября', 'Декабря'
        ],
        'en_EN'=>[
            'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'
        ]
    ];


    public static function getPupleDate( $date, $format = 'd {month} Y' )
    {
        $time = strtotime( $date);
        return self::getDateFormat($time, $format);
    }

    public static function getPupleDatetime( $date, $format = 'd {month} Y H:i' )
    {
        $time = strtotime( $date );
        return self::getDateFormat($time, $format);
    }


    public static function getDateFormat( $time, $format ){
        $month = date('m',$time);

        $format = str_replace('{month}',self::$month[\Yii::$app->language][$month-1],$format);

        return date($format, $time);
    }

    /**
     * Функция для определения окончаний у чисел.
     *
     * использование:
     * echo $this->morph(count($this->comments), "комментарий", "комментария", "комментариев")
     *
     * @param $value
     * @param $word0
     * @param $word1
     * @param $word2
     * @param string $separator
     * @return string
     */
    public function morph($value, $word0, $word1, $word2, $separator = '&nbsp;')
    {
        if (preg_match('/1\d$/', $value)) {
            return $separator.$word2;
        } elseif (preg_match('/1$/', $value)) {
            return $separator.$word0;
        } elseif (preg_match('/(2|3|4)$/', $value)) {
            return $separator.$word1;
        } else {
            return $separator.$word2;
        }
    }

} 