<?php

namespace emilasp\core;

use emilasp\core\components\OptionsData;

class CoreModule extends \yii\base\Module
{
    //public $controllerNamespace = 'emilasp\core\controllers';

    public $enableThemes = false;

    /**
     * @var OptionsData
     */
    public $h_options;

    public $theme = 'default';

    public function init()
    {
        parent::init();

        if($this->enableThemes)  $this->setTheme();

        $this->h_options = new OptionsData();

        // custom initialization code goes here
    }

    private function setTheme(){
        $this->setViewPath('@app/themes/'.$this->theme.'/modules/'.$this->id);
    }

}
